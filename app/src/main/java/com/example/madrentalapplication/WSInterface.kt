package com.example.madrentalapplication

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WSInterface {
    @GET("exchange/madrental/get-vehicules.php")
    fun getCars(): Call<List<ReturnWSGet>>
}