package com.example.madrentalapplication

import com.google.gson.annotations.SerializedName

data class ReturnWSGet(
    val id: Int,
    val nom: String,
    val image: String,
    val disponible: Int,
    val prixjournalierbase: Int,
    val promotion: Int,
    val agemin: Int,
    val categorieco2: String,
    val equipements: List<Equipment>,
    val options:  List<Option>)

data class Equipment(
    val id: Int,
    val nom: String)

data class Option(
    val id: Int,
    val nom: String,
    val price: Int)