package com.example.madrentalapplication

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [CarDTO::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun carsDAO(): CarsDAO
}