package com.example.madrentalapplication

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="cars")
class CarDTO(
    @PrimaryKey(autoGenerate = true)
    val carId: Long = 0,
    val name: String,
    val image: String,
    val price: Int,
    val category: String)