package com.example.madrentalapplication

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
abstract class CarsDAO {
    @Query("SELECT * FROM cars")
    abstract fun getListCars(): List<CarDTO>

    @Insert
    abstract fun insert(vararg cars: CarDTO)

    @Delete
    abstract fun delete(vararg cars: CarDTO)
}