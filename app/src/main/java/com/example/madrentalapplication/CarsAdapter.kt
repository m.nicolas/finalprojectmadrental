package com.example.madrentalapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CarsAdapter (private var listCars: MutableList<Car>) :
    RecyclerView.Adapter<CarsAdapter.CarViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val viewCourse = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_car, parent, false)
        return CarViewHolder(viewCourse)

    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.textViewNameCar.text = listCars[position].name
        holder.textViewPriceCar.text = listCars[position].price.toString()
        holder.textViewCategoryCar.text = listCars[position].category
    }

    override fun getItemCount(): Int = listCars.size

    inner class CarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewNameCar: TextView = itemView.findViewById(R.id.car_name)
        val textViewPriceCar: TextView = itemView.findViewById(R.id.car_price)
        val textViewCategoryCar: TextView = itemView.findViewById(R.id.car_category)
    }
}